/*----------------------------------*/
/* OL_web_client_JS.js				*/
/* UNIGIS MSc Dissertation web tool	*/
/* Date: 16/01/2017					*/
/* Author: Eddie Boyle				*/
/*----------------------------------*/

//OpenLayers map
function OL_map() {
	//add OSM and GeoServer WMS layers
	var layers = [
  		new ol.layer.Tile({
   			source: new ol.source.OSM(),
   			title: 'OSM',
   			description: 'OpenStreetMap base layer'
  		}),
       	new ol.layer.Image({
       		source: new ol.source.ImageWMS({
           		url: 'http://52.209.201.41:8080/geoserver/cite/wms',
           		params: {'LAYERS': 'cite:CNP_roads'},
           		serverType: 'geoserver',
           		crossOrigin: ''
       		}),
       		opacity: 0.25,
       		title: 'Remoteness',
       		description: 'Remoteness from mechanised access; distance from constructed vehicular access routes (roads)'
  		}),
  		new ol.layer.Image({
           	source: new ol.source.ImageWMS({
               	url: 'http://52.209.201.41:8080/geoserver/cite/wms',
               	params: {'LAYERS': 'cite:CNP_dem3'},
               	serverType: 'geoserver',
               	crossOrigin: ''
           	}),
           	opacity: 0.25,
           	title: 'Ruggedness',
           	description: 'Rugged terrain; physically challenging terrain including effects of steep and rough terrain and harsh weather conditions often found at higher altitudes'
		}),
 		new ol.layer.Image({
		    source: new ol.source.ImageWMS({
		        url: 'http://52.209.201.41:8080/geoserver/cite/wms',
		        params: {'LAYERS': 'cite:CNP_buildings'},
		        serverType: 'geoserver',
		    	crossOrigin: ''
		    }),
			opacity: 0.25,
			title: 'Absence',
			description: 'Absence of modern human artefacts; the degree to which the landscape is free from the presence of the permanent structures of modern society, i.e. buildings'
  		}),
     	new ol.layer.Image({
       		source: new ol.source.ImageWMS({
         		url: 'http://52.209.201.41:8080/geoserver/cite/wms',
         		params: {'LAYERS': 'cite:CNP_landcover'},
         		serverType: 'geoserver',
         		crossOrigin: ''
         	}),
       		opacity: 0.25,
       		title: 'Naturalness',
       		description: 'Naturalness of land cover; the degree to which the natural environment is free of biophysical disturbances due to the influence of modern society; based on land cover classifications and weightings'
 		})
	];
	var map = new ol.Map({
  		layers: layers,
  		target: 'map',
  		view: new ol.View({
    		center: ol.proj.fromLonLat([-3.6, 57.0]),
    		zoom: 9
  		}),
  		controls: ol.control.defaults().extend([	// Add a new Layerswitcher to the map
			new ol.control.LayerSwitcher({
				oninfo: function (l) { alert(l.get("description")); }
			})
		])
	});

	//Enable export PNG of map

	var exportPNGElement = document.getElementById('export-png');
	if ('download' in exportPNGElement) {
		exportPNGElement.addEventListener('click', function(e) {
	    	map.once('postcompose', function(event) {
	        	var canvas = event.context.canvas;
	        	exportPNGElement.href = canvas.toDataURL('image/png');
	      	});
	      	map.renderSync();
	    },false);
	}
	else {
		var info = document.getElementById('no-download');
	    //display error message
	    info.style.display = '';
	}
}

//Help slider window
jQuery(document).ready(function($){
    $("#help-slide").click(function() {
        $(this).stop().animate({right: '-500px'});
    });
});

jQuery(document).ready(function($){
    $(".open-help").click(function() {
        $("#help-slide").stop().animate({right: '0px'});
    });
});
