/*----------------------------------*/
/* OL_web_client_JS.js				*/
/* UNIGIS MSc Dissertation web tool	*/
/* Date: 16/01/2017					*/
/* Author: Eddie Boyle				*/
/*----------------------------------*/

//OpenLayers map
function OL_map() {
	//add OSM and GeoServer WMS layers
	var layers = [
  		new ol.layer.Tile({
   			source: new ol.source.OSM()
  		}),
    	new ol.layer.Image({
      		source: new ol.source.ImageWMS({
        		url: 'http://52.209.201.41:8080/geoserver/cite/wms',
        		params: {'LAYERS': 'cite:CNP_landcover'},
        		serverType: 'geoserver',
        		crossOrigin: ''
        	}),
      		opacity: 0.6
 		}),
       	new ol.layer.Image({
       		source: new ol.source.ImageWMS({
           		url: 'http://52.209.201.41:8080/geoserver/cite/wms',
           		params: {'LAYERS': 'cite:CNP_roads'},
           		serverType: 'geoserver',
           		crossOrigin: ''
       		}),
       		opacity: 0.6
  		}),
       	new ol.layer.Image({
           	source: new ol.source.ImageWMS({
           		url: 'http://52.209.201.41:8080/geoserver/cite/wms',
           		params: {'LAYERS': 'cite:CNP_buildings'},
           		serverType: 'geoserver',
           		crossOrigin: ''
          	}),
           	opacity: 0.6
  		}),
  		new ol.layer.Image({
           	source: new ol.source.ImageWMS({
               	url: 'http://52.209.201.41:8080/geoserver/cite/wms',
               	params: {'LAYERS': 'cite:CNP_dem3'},
               	serverType: 'geoserver',
               	crossOrigin: ''
           	}),
           	opacity: 0.6
		})
	];
	var map = new ol.Map({
  		layers: layers,
  		target: 'map',
  		view: new ol.View({
    		center: ol.proj.fromLonLat([-3.6, 57.0]),
    		zoom: 9
  		})
	});

	//Enable export PNG of map

	var exportPNGElement = document.getElementById('export-png');
	if ('download' in exportPNGElement) {
		exportPNGElement.addEventListener('click', function(e) {
	    	map.once('postcompose', function(event) {
	        	var canvas = event.context.canvas;
	        	exportPNGElement.href = canvas.toDataURL('image/png');
	      	});
	      	map.renderSync();
	    },false);
	}
	else {
		var info = document.getElementById('no-download');
	    //display error message
	    info.style.display = '';
	}
}

//Help slider window
jQuery(document).ready(function($){
    $("#help-slide").click(function() {
        $(this).stop().animate({right: '-500px'});
    });
});

jQuery(document).ready(function($){
    $(".open-help").click(function() {
        $("#help-slide").stop().animate({right: '0px'});
    });
});
