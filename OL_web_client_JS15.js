/*----------------------------------*/
/* OL_web_client_JS.js				*/
/* UNIGIS MSc Dissertation web tool	*/
/* Date: 16/01/2017					*/
/* Author: Eddie Boyle				*/
/*----------------------------------*/

//OpenLayers map
function OL_map() {
	//add GeoServer WMS layers
       	var layer_remoteness = new ol.layer.Image({
       		source: new ol.source.ImageWMS({
           		url: 'http://52.209.201.41:8080/geoserver/cite/wms',
           		params: {'LAYERS': 'cite:CNP_remote1'},
           		serverType: 'geoserver',
           		crossOrigin: ''
       		}),
       		opacity: 0.25,
       		title: 'Remoteness',
       		description: 'Remoteness from mechanised access; distance from constructed vehicular access routes (roads)'
  		});
  		var layer_ruggedness = new ol.layer.Image({
           	source: new ol.source.ImageWMS({
               	url: 'http://52.209.201.41:8080/geoserver/cite/wms',
               	params: {'LAYERS': 'cite:CNP_ruggedness1'},
               	serverType: 'geoserver',
               	crossOrigin: ''
           	}),
           	opacity: 0.25,
           	title: 'Ruggedness',
           	description: 'Rugged terrain; physically challenging terrain including effects of steep and rough terrain and harsh weather conditions often found at higher altitudes'
		});
 		var layer_absence = new ol.layer.Image({
		    source: new ol.source.ImageWMS({
		        url: 'http://52.209.201.41:8080/geoserver/cite/wms',
		        //params: {'LAYERS': 'cite:CNP_absence1'},
		       	params: {'LAYERS': 'cite:CNP_absence2'},
		        serverType: 'geoserver',
		    	crossOrigin: ''
		    }),
			opacity: 0.25,
			title: 'Absence',
			description: 'Absence of modern human artefacts; the degree to which the landscape is free from the presence of the permanent structures of modern society, i.e. buildings'
  		});
     	var layer_naturalness = new ol.layer.Image({
       		source: new ol.source.ImageWMS({
         		url: 'http://52.209.201.41:8080/geoserver/cite/wms',
         		params: {'LAYERS': 'cite:CNP_naturalness1'},
         		serverType: 'geoserver',
         		crossOrigin: ''
         	}),
       		opacity: 0.25,
       		title: 'Naturalness',
       		description: 'Naturalness of land cover; the degree to which the natural environment is free of biophysical disturbances due to the influence of modern society; based on land cover classifications and weightings'
 		});

	// A group layer for OSM and GeoServer SNH base layers
	var baseLayers = new ol.layer.Group({
		title: 'Base Layers',
		description: 'Base layer maps',
		openInLayerSwitcher: false,
		layers:
		[
			new ol.layer.Image({
				baseLayer: true,
	        	source: new ol.source.ImageWMS({
	            	url: 'http://52.209.201.41:8080/geoserver/cite/wms',
	            	params: {'LAYERS': 'cite:WILDLAND_SCOTLAND'},
	            	serverType: 'geoserver',
	            	crossOrigin: ''
	        		}),
	        	opacity: 0.25,
	        	visible: false,
	        	title: 'SNH Wild Land',
      			description: 'SNH Wild Land areas 2014'
			}),
     		new ol.layer.Image({
				baseLayer: true,
     			source: new ol.source.ImageWMS({
         			url: 'http://52.209.201.41:8080/geoserver/cite/wms',
         			params: {'LAYERS': 'cite:WILDNESS-CMP_SCOTLAND'},
         			serverType: 'geoserver',
         			crossOrigin: ''
     			}),
     			opacity: 0.25,
	        	visible: false,
     			title: 'SNH Wildness',
     			description: 'SNH Wildness map of Scotland'
			}),
			new ol.layer.Tile({
				baseLayer: true,
   				source: new ol.source.OSM(),
   				opacity: 0.7,
   				visible: true,
   				title: 'OSM',
   				description: 'OpenStreetMap'
			})
		]
	});

	var map = new ol.Map({
  		layers: [ baseLayers, layer_remoteness, layer_ruggedness, layer_absence, layer_naturalness ],
  		target: 'map',
  		view: new ol.View({
    		center: ol.proj.fromLonLat([-3.6, 57.0]),
    		zoom: 9
  		}),
  		controls: ol.control.defaults().extend([	// Add a scale bar and a new Layerswitcher to the map
  			new ol.control.ScaleLine(),
			new ol.control.LayerSwitcher({
				oninfo: function (l) { alert(l.get("description")); }
			})
		])
	});

	//Enable multiplicative alpha blending of layer opacities - note: this does not work in IE browsers
	map.on('precompose', function(evt) {
  		evt.context.globalCompositeOperation = "multiply";
	});

	//Enable export PNG of map - note: this does not work in IE browsers
	var exportPNGElement = document.getElementById('export-png');
	if ('download' in exportPNGElement) {
		exportPNGElement.addEventListener('click', function(e) {
	    	map.once('postcompose', function(event) {
	        	var canvas = event.context.canvas;
	        	exportPNGElement.href = canvas.toDataURL('image/png');
	      	});
	      	map.renderSync();
	    },false);
	}
	else {
		var info = document.getElementById('no-download');
	    //display error message
	    info.style.display = '';
	}
}

//Data slider window
jQuery(document).ready(function($){
    $("#data-slide").click(function() {
        $(this).stop().animate({right: '-500px'});
    });
});

jQuery(document).ready(function($){
    $(".open-data").click(function() {
        $("#data-slide").stop().animate({right: '0px'});
    });
});

//Help slider window
jQuery(document).ready(function($){
    $("#help-slide").click(function() {
        $(this).stop().animate({left: '-500px'});
    });
});

jQuery(document).ready(function($){
    $(".open-help").click(function() {
        $("#help-slide").stop().animate({left: '0px'});
    });
});