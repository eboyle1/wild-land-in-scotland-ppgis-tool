/*----------------------------------*/
/* OL_web_client_JS.js				*/
/* UNIGIS MSc Dissertation web tool	*/
/* Date: 16/01/2017					*/
/* Author: Eddie Boyle				*/
/*----------------------------------*/

//OpenLayers map
function OL_map() {

	//Use OS OpenSpace basemap (for main map and overview map), set projection for all maps to EPSG:27700
    var openSpaceOl3_main = new OpenSpaceOl3_main('49D29E9E9ED43342E0530C6CA40ADCB9', document.URL, OpenSpaceOl3_main.ALL_LAYERS);
    var openSpaceOl3_overview = new OpenSpaceOl3_overview('49D29E9E9ED43342E0530C6CA40ADCB9', document.URL, OpenSpaceOl3_overview.ALL_LAYERS);
    var extent = ol.proj.transformExtent([-8.74, 49.81, 1.84, 60.9], 'EPSG:4326', 'EPSG:27700');
	var projection = ol.proj.get('EPSG:27700');
    projection.setExtent(extent);

	//add GeoServer WMS layers
    var layer_remoteness = new ol.layer.Image({
    	source: new ol.source.ImageWMS({
			projection: projection,
       		url: 'http://52.209.201.41:8080/geoserver/cite/wms',
       		params: {'LAYERS': 'cite:CNP_remote3'},
       		serverType: 'geoserver',
       		crossOrigin: ''
   		}),
   		opacity: 0.25,
   		title: 'Remoteness',
   		description: 'Remoteness from mechanised access; distance from constructed vehicular access routes (roads)'
	});
	var layer_ruggedness = new ol.layer.Image({
       	source: new ol.source.ImageWMS({
			projection: projection,
	       	url: 'http://52.209.201.41:8080/geoserver/cite/wms',
	       	params: {'LAYERS': 'cite:CNP_ruggedness3'},
	       	serverType: 'geoserver',
	       	crossOrigin: ''
       	}),
       	opacity: 0.25,
       	title: 'Ruggedness',
       	description: 'Rugged terrain; physically challenging terrain including effects of steep and rough terrain and harsh weather conditions often found at higher altitudes'
	});
 	var layer_absence = new ol.layer.Image({
	    source: new ol.source.ImageWMS({
			projection: projection,
	        url: 'http://52.209.201.41:8080/geoserver/cite/wms',
	       	params: {'LAYERS': 'cite:CNP_absence3'},
	        serverType: 'geoserver',
	    	crossOrigin: ''
	    }),
		opacity: 0.25,
		title: 'Absence',
		description: 'Absence of modern human artefacts; the degree to which the landscape is free from the presence of the permanent structures of modern society, i.e. buildings'
  	});
   	var layer_naturalness = new ol.layer.Image({
   		source: new ol.source.ImageWMS({
			projection: projection,
    		url: 'http://52.209.201.41:8080/geoserver/cite/wms',
       		params: {'LAYERS': 'cite:CNP_naturalness3'},
       		serverType: 'geoserver',
       		crossOrigin: ''
       	}),
   		opacity: 0.25,
   		title: 'Naturalness',
   		description: 'Naturalness of land cover; the degree to which the natural environment is free of biophysical disturbances due to the influence of modern society; based on land cover classifications and weightings'
	});

	//OSM layer, unused currently
	var layer_OSM = new ol.layer.Tile({
		baseLayer: true,
   		source: new ol.source.OSM(),
   		opacity: 1.0,
   		visible: true,
   		title: 'OSM',
   		description: 'OpenStreetMap'
	})

	//main map layer and view
	var layer_OS1 = openSpaceOl3_main.getLayer();
	var view1 = new ol.View({
			projection: projection,
    		center: ol.proj.fromLonLat([-3.55, 57.05], projection),
    		maxResolution: 5000,
    		zoom: 5
  			});
  	//overview map layer and view
  	var layer_OS2 = openSpaceOl3_overview.getLayer();
	var view2 = new ol.View({
   			projection: projection,
   			center: ol.proj.fromLonLat([-3.55, 57.05], projection),
			extent: extent,
			maxResolution: 2500
   			});

	// A group layer for OS OpenSpace and GeoServer SNH base layers
	var baseLayers = new ol.layer.Group({
		title: 'Base Layers',
		description: 'Base layer maps',
		openInLayerSwitcher: false,
		layers:
		[
			new ol.layer.Image({
				baseLayer: true,
	        	source: new ol.source.ImageWMS({
					projection: projection,
	            	url: 'http://52.209.201.41:8080/geoserver/cite/wms',
	            	params: {'LAYERS': 'cite:WILDLAND_SCOTLAND'},
	            	serverType: 'geoserver',
	            	crossOrigin: ''
	        		}),
	        	opacity: 0.25,
	        	visible: false,
	        	title: 'SNH Wild Land',
      			description: 'SNH Wild Land areas 2014'
			}),
     		new ol.layer.Image({
				baseLayer: true,
     			source: new ol.source.ImageWMS({
					projection: projection,
         			url: 'http://52.209.201.41:8080/geoserver/cite/wms',
         			params: {'LAYERS': 'cite:WILDNESS-CMP_SCOTLAND'},
         			serverType: 'geoserver',
         			crossOrigin: ''
     			}),
     			opacity: 0.25,
	        	visible: false,
     			title: 'SNH Wildness',
     			description: 'SNH Wildness map of Scotland'
			}),
			layer_OS1
		]
	});

	var map = new ol.Map({
  		layers: [ baseLayers, layer_remoteness, layer_ruggedness, layer_absence, layer_naturalness ],
  		target: 'map',
  		logo: false,
  		view: view1,
     	controls: ol.control.defaults({
        	attributionOptions: ({
        		collapsible: false
        	})
    	}).extend([
			// Add a scale bar, layer switcher, custom extent button,overview map and OS logo to the map
  			new ol.control.ScaleLine(),
			new OpenSpaceOl3_main.OpenSpaceLogoControl({ className: 'openspaceol3-openspace-logo' }),
			new ol.control.OverviewMap({
				layers: [ layer_OS2 ],
//				layers: [ layer_OSM ],
                collapsed: false,
                view: view2
            }),
    		new ol.control.ZoomToExtent({
        		label:'NP',
        		className: 'customExtentZoomClass',
        		tipLabel : 'Custom image button',
      			extent: [249588.32,762436.42,351547.78,836671.46]
    		}),
			new ol.control.LayerSwitcher({
				oninfo: function (l) { alert(l.get("description")); }
			})
		])
	});

	//Enable multiplicative alpha blending of layer opacities - note: this does not work in IE browsers
	map.on('precompose', function(evt) {
  		evt.context.globalCompositeOperation = "multiply";
	});

	//Enable export PNG of map - note: this does not work in IE browsers
	var exportPNGElement = document.getElementById('export-png');
	if ('download' in exportPNGElement) {
		exportPNGElement.addEventListener('click', function(e) {
	    	map.once('postcompose', function(event) {
	        	var canvas = event.context.canvas;
	        	exportPNGElement.href = canvas.toDataURL('image/png');
	      	});
	      	map.renderSync();
	    },false);
	}
	else {
		var info = document.getElementById('no-download');
	    //display error message
	    info.style.display = '';
	}
}

//Data slider window
jQuery(document).ready(function($){
    $("#data-slide").click(function() {
        $(this).stop().animate({right: '-500px'});
    });
});

jQuery(document).ready(function($){
    $(".open-data").click(function() {
        $("#data-slide").stop().animate({right: '0px'});
    });
});

//Help slider window
jQuery(document).ready(function($){
    $("#help-slide").click(function() {
        $(this).stop().animate({left: '-500px'});
    });
});

jQuery(document).ready(function($){
    $(".open-help").click(function() {
        $("#help-slide").stop().animate({left: '0px'});
    });
});